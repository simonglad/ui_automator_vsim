package com.pltsci.vsim

import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import androidx.test.core.app.ApplicationProvider.getApplicationContext
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.uiautomator.By
import androidx.test.uiautomator.UiDevice
import androidx.test.uiautomator.UiObject2
import androidx.test.uiautomator.Until
import org.junit.After
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

private const val CM_PACKAGE_NAME = "com.pltsci.connection"
private const val VEHICLE_PACKAGE_NAME = "$CM_PACKAGE_NAME.vehicle"
private const val LAUNCH_TIMEOUT = 5000L
private const val STRING_TO_BE_TYPED = "VSIM_VEHICLE_ID"
private const val SHORT_TIMEOUT: Long = 500

@RunWith(AndroidJUnit4::class)
class Automation {

    private val device: UiDevice

    init {
        val instrumentation = InstrumentationRegistry.getInstrumentation()
        device = UiDevice.getInstance(instrumentation)
    }

    @Before
    fun setUp() {
        // Press Home key before running the test
        device.pressHome()
    }

    @After
    fun tearDown() {
        // Press Home key after running the test
        device.pressHome()
    }

    @Test
    fun testConnectToVehicle() {
        startCMFromHomeScreen()

        device.findObject(By.res(CM_PACKAGE_NAME, "vehicleButton"))?.click() ?: Assert.fail()
        // Verify the test is displayed in the Ui
        val changedText: UiObject2 = device
            .wait(
                Until.findObject(By.res(VEHICLE_PACKAGE_NAME, "vehicleIdEditText")),
                SHORT_TIMEOUT /* wait 500ms */
            )
        changedText.text = STRING_TO_BE_TYPED
        device.pressBack()
        device.findObject(By.res(VEHICLE_PACKAGE_NAME, "connectButton"))?.click() ?: Assert.fail()
    }

    @Test
    fun validateWIM() {
        Assert.assertTrue(isWim())
    }

    private fun startCMFromHomeScreen() {
        // Wait for launcher
        val launcherPackage: String = getLauncherPackageName() ?: return
        device.wait(Until.hasObject(By.pkg(launcherPackage).depth(0)), LAUNCH_TIMEOUT)

        // Launch the CM app
        val context: Context = getApplicationContext()
        val intent: Intent = context.packageManager
            .getLaunchIntentForPackage(CM_PACKAGE_NAME) ?: return
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK) // Clear out any previous instances
        context.startActivity(intent)

        // Wait for the app to appear
        device.wait(Until.hasObject(By.pkg(CM_PACKAGE_NAME).depth(0)), LAUNCH_TIMEOUT)
    }

    /**
     * Uses package manager to find the package name of the device launcher. Usually this package
     * is "com.android.launcher" but can be different at times. This is a generic solution which
     * works on all platforms.`
     */
    private fun getLauncherPackageName(): String? {
        // Create launcher Intent
        val intent = Intent(Intent.ACTION_MAIN)
        intent.addCategory(Intent.CATEGORY_HOME)

        // Use PackageManager to get the launcher package name
        val pm = getApplicationContext<Context>().packageManager
        val resolveInfo = pm.resolveActivity(intent, PackageManager.MATCH_DEFAULT_ONLY)
        return resolveInfo?.activityInfo?.packageName
    }

    private fun isWim() = device.hasObject(By.text("Device locked while driving"))
}
